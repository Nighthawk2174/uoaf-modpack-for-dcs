RIM_116A =
{
	category		= CAT_MISSILES,
	name			= "RIM_116A",
	user_name		= _("RIM-116A"),
	scheme			= "passive_plus_IR_homing_missile",
	class_name		= "wAmmunitionSelfHoming",
	model			= "rim-116",
	mass			= 73.6,
	
	wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_SA_Missile,WSTYPE_PLACEHOLDER},

	Escort			= 0,
	Head_Type		= 3,
	sigma			= {1, 1, 1},
	M				= 73.6,
	H_max			= 4000.0,
	H_min			= -1,
	Diam			= 127.0,
	Cx_pil			= 1,
	D_max			= 10000.0,
	D_min			= 500.0,
	Head_Form		= 0,
	Life_Time		= 24,
	Nr_max			= 20,
	v_min			= 140.0,
	v_mid			= 350.0,
	Mach_max		= 2.5,
	t_b				= 0.0,
	t_acc			= 5.0,
	t_marsh			= 0.0,
	Range_max		= 10000.0,
	H_min_t			= 1.0,
	Fi_start		= math.rad(1),
	Fi_rak			= 3.14152,
	Fi_excort		= 0.79,
	Fi_search		= 99.9,
	OmViz_max		= 99.9,
	X_back			= 0,
	Y_back			= 0,
	Z_back			= 0,
	Reflection		= 0.0182,
	KillDistance	= 5.0,

	shape_table_data =
	{
		{
			name		= "RIM_116A",
			file		= "rim-116",
			life		= 1,
			fire		= { 0, 1},
			username	= _("RIM-116A"),
			index		= WSTYPE_PLACEHOLDER,
		},
	},
	
	controller = {
		boost_start = 0.001,
		march_start = 0.201,
	},
	
	booster = {
		impulse								= 160,
		fuel_mass							= 0.5,
		work_time							= 0.2,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.41, 0.0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.3,				
	},
		
	march = {
		impulse								= 265,
		fuel_mass							= 27.4,
		work_time							= 5,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.41, 0.0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.2,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.3,	
	},

	fm = {
		mass        = 73.6,  
		caliber     = 0.127,  
		cx_coeff    = {1,2.7,0.8,1.36,1.75},
		L           = 2.82,
		I           = 1 / 12 * 73.6 * 2.82 * 2.82,
		Ma          = 0.6,
		Mw          = 1.2,
		Sw			= 0.22,
		dCydA		= {0.07, 0.036},
		A			= 0.6,
		maxAoa		= 0.3,
		finsTau		= 0.1,
		freq		= 15,
	},
	
	
	seeker = {
		delay			= 0.06,
		FOV				= math.rad(7)*2,
		max_w_LOS		= 999,
		aim_sigma 		= 20,
	},
	
	IR_seeker = {
		sensitivity		= 10500,
		cooled			= true,
		delay			= 0.04,
		GimbLim			= math.rad(130),
		FOV				= math.rad(7)*2,
		opTime			= 45.0,
		target_H_min	= 0.0,
		flag_dist		= 150.0,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed	= 1,
		radius				= 5,
	},
	
	autopilot = {
		K				= 60.0,
		Kg				= 2.4,
		Ki				= 0.0,
		finsLimit		= 0.4,
		delay 			= 0.5,
		fin2_coeff		= 0.5,
	},
	
	
	warhead = predefined_warhead("RAM"),
	warhead_air = predefined_warhead("RAM")
}

declare_weapon(RIM_116A)

GT_t.WS_t.RIM_116 = {} -- RIM-116
GT_t.WS_t.RIM_116.angles = {
					{math.rad(180), math.rad(-180), math.rad(-15), math.rad(85)},
					};
GT_t.WS_t.RIM_116.omegaY = 1
GT_t.WS_t.RIM_116.omegaZ = 1
GT_t.WS_t.RIM_116.distanceMin = 1000
GT_t.WS_t.RIM_116.distanceMax = 11000
GT_t.WS_t.RIM_116.ECM_K = 0.8;
GT_t.WS_t.RIM_116.reference_angle_Z = 0
GT_t.WS_t.RIM_116.LN = {}
GT_t.WS_t.RIM_116.LN[1] = {}
GT_t.WS_t.RIM_116.LN[1].type = 4
GT_t.WS_t.RIM_116.LN[1].launch_delay = 0.5;
GT_t.WS_t.RIM_116.LN[1].reactionTime = 0.1;
GT_t.WS_t.RIM_116.LN[1].reflection_limit = 0.02;
GT_t.WS_t.RIM_116.LN[1].sensor = {}
set_recursive_metatable(GT_t.WS_t.RIM_116.LN[1].sensor, GT_t.WSN_t[0])
GT_t.WS_t.RIM_116.LN[1].beamWidth = math.rad(90);
GT_t.WS_t.RIM_116.LN[1].BR = { {pos = {0, 0, 0.9} }, {pos = {0, 0, -0.9} } }
GT_t.WS_t.RIM_116.LN[1].PL = {}
GT_t.WS_t.RIM_116.LN[1].PL[1] = {}
GT_t.WS_t.RIM_116.LN[1].PL[1].ammo_capacity = 21
GT_t.WS_t.RIM_116.LN[1].PL[1].type_ammunition = RIM_116A.wsTypeOfWeapon;
GT_t.WS_t.RIM_116.LN[1].PL[1].shot_delay = 0.1;
GT_t.WS_t.RIM_116.LN[1].PL[1].reload_time = 1000000; -- never during the mission
