
stroke_thickness  = 0.75
stroke_fuzziness  = 0.15

-- Currently is used for DMC generated fonts black outline
DMC_outline_thickness = stroke_thickness * 2
DMC_outline_fuzziness = stroke_fuzziness * 0.6
DMC_stroke_thickness  = 0.75
DMC_stroke_fuzziness  = 0.15
